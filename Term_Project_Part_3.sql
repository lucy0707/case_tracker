
--spool TermProject_Part3.lst
pSool TermProject_Part32.lst




REM TermProject_Part3  � CS 276
REM Lucia Castaldo
SET FEEDBACK ON;
SET ECHO ON;
SET LINESIZE 300;
SET PAGESIZE 250;
SET SERVEROUTPUT ON


DROP TRIGGER name_change_trg;
DROP TRIGGER medlog_trg;

DROP SEQUENCE Emp_sequ;
CREATE SEQUENCE Emp_sequ start with 10;

DROP TABLE Hospital_Emp;

CREATE TABLE Hospital_Emp
(EmployeeID NUMBER(6) NOT NULL PRIMARY KEY,
FirstName VARCHAR(8) NOT NULL,
LastName VARCHAR(20) NOT NULL
);



DROP SEQUENCE MedLog_seq;
CREATE SEQUENCE MedLog_seq start with 1;

DROP TABLE MEDICATION_LOG;

	CREATE TABLE MEDICATION_LOG
    (MedlogID VARCHAR(6) NOT NULL PRIMARY KEY,
	 DeletedTime DATE NOT NULL,
	 MedtherapyID NUMBER NOT NULL,
	 CONSTRAINT medlog_caseid_fk FOREIGN KEY(MedtherapyID)
  REFERENCES Medication_therapy(MedtherapyID)
      );

CREATE TABLE ANESTHESIA_LOG
(ProviderID VARCHAR2(6)NOT NULL,
CaseID	NUMBER(10),
StartTime DATE,
EndTime	DATE ,
 PRIMARY KEY(StartTime,CaseID,ProviderID),
 CONSTRAINT AnLog_caseid_fk FOREIGN KEY(CaseID)
  REFERENCES CASE(CaseID)
);
DROP TABLE MEDICATION_THERAPY;
DROP TABLE ANESTHESIA_LOG;
DROP TABLE VITAL_SIGNS;

CREATE TABLE CASE
     (CaseID NUMBER(10),
     PatientID VARCHAR(12)NOT NULL,
  SurgeonID VARCHAR2(6)NOT NULL,
    casedate DATE NOT NULL,
     FacilityID number(10) NOT NULL ,
    ORnumber NUMBER(10)NOT NULL,
      ProcedureID VARCHAR(8) NOT NULL,
       CONSTRAINT case_id_pk PRIMARY KEY (CaseID),
       CONSTRAINT case_patientid_fk FOREIGN KEY (PatientID)
           REFERENCES PATIENT (PatientID),
   CONSTRAINT case_surgeonid_fk FOREIGN KEY(SurgeonID)
   REFERENCES  SURGEON(SurgeonID),
   CONSTRAINT case_facilityid_fk FOREIGN KEY(FacilityID)
    REFERENCES FACILITY(FacilityID)
    );
	
	DROP TABLE MEDICATION;
 
  CREATE TABLE MEDICATION(
   MedicationID VARCHAR(10)NOT NULL,
  Name VARCHAR2(30)NOT NULL,
   Concentration varchar(20),
   CONSTRAINT med_id_pk PRIMARY KEY(MedicationID)
  );
  
  

CREATE TABLE VITAL_SIGNS
(vitalsTime TIMESTAMP NOT NULL,
  CaseID NUMBER(10) NOT NULL,
  heart_rate NUMBER(3)NOT NULL,
   SpO2 NUMBER(3)NOT NULL,
 bp_systolic NUMBER(3)NOT NULL,
    bp_diastolic NUMBER(3)NOT NULL,
  PRIMARY KEY(vitalsTime,CaseID),
   CONSTRAINT vs_caseid_FK FOREIGN KEY(CaseID)
  REFERENCES CASE(CaseID)
  );
  
  

ALTER TABLE VITAL_SIGNS
MODIFY (heart_rate NUMBER (3) );

	-- i created it again to change datetime instead of timestamp
CREATE TABLE MEDICATION_THERAPY
  (MedtherapyID NUMBER NOT NULL,
  medTime DATE NOT NULL ,
  CaseID NUMBER(10) NOT NULL,
   dosage NUMBER(5,1) NOT NULL ,
   units VARCHAR(10)NOT NULL,
   MedicationID VARCHAR2(10) NOT NULL,
  route VARCHAR2(10) DEFAULT 'IV',
   PRIMARY KEY(MedtherapyID),
   CONSTRAINT ther_caseid_FK FOREIGN KEY(CaseID)
    REFERENCES CASE(CaseID),
  CONSTRAINT ther_medid_FK FOREIGN KEY(MedicationID)
   REFERENCES MEDICATION (MedicationID)
  );
  
  
CREATE TABLE NAMECHANGE_LOG
(LogID  VARCHAR(8)NOT NULL PRIMARY KEY,
PatientID VARCHAR(12)NOT NULL,
FName VARCHAR(20) NOT NULL,
LName VARCHAR (20)NOT NULL,
ChangeDate DATE NOT NULL,
 CONSTRAINT nl_patientid_FK FOREIGN KEY(PatientID)
   REFERENCES PATIENT(PatientID) ON DELETE CASCADE
);
 
 

DROP SEQUENCE LogID_seq;
CREATE SEQUENCE LogID_seq start with 2000;


 --INSERT INTO MEDICATION
 INSERT INTO MEDICATION(medicationid,name,concentration)
 VALUES('roc','rocuronium','mg/ml');
 
	

	
 --INSERT INTO MEDICATION
 INSERT INTO MEDICATION(medicationid,name,concentration)
 VALUES('roc','rocuronium','mg/ml');
 
 INSERT INTO MEDICATION(medicationid,name,concentration)
 VALUES('fen','fentanyl','mg/ml');
 
 
 INSERT INTO MEDICATION(medicationid,name,concentration)
 VALUES('mor','morphine','mg/ml');
 
 
 INSERT INTO MEDICATION(medicationid,name,concentration)
 VALUES('neo','phenylephrine','mg/ml');
 
 
 INSERT INTO MEDICATION(medicationid,name,concentration)
 VALUES('eph','ephedrine','mg/ml');
 
 
 INSERT INTO MEDICATION(medicationid,name,concentration)
 VALUES('atr','atropine','mg/ml');
 
 
 INSERT INTO MEDICATION(medicationid,name,concentration)
 VALUES('gly','glycopyrolate','mg/ml');
 
 
 INSERT INTO MEDICATION(medicationid,name,concentration)
 VALUES('ngm','neostigmine','mg/ml');
 
 
 
 INSERT INTO MEDICATION(medicationid,name,concentration)
 VALUES('dro','droperidol','mg/ml');
 
 
 INSERT INTO MEDICATION(medicationid,name,concentration)
 VALUES('ond','ondasteron','mg/ml');
 
 
 INSERT INTO MEDICATION(medicationid,name,concentration)
 VALUES('pro','propoful','mg/ml');
 
 
 INSERT INTO MEDICATION(medicationid,name,concentration)
 VALUES('lid','lidocain','mg/ml');
 
 
 INSERT INTO MEDICATION(medicationid,name,concentration)
 VALUES('dex','dexamethasone','mg/ml');
  
 --INSERT INTO MEDICATION
 INSERT INTO MEDICATION(medicationid,name,concentration)
 VALUES('roc','rocuronium','mg/ml');
 
 DROP TABLE SURGEON;
 
	
	--Inserting values into surgeon table
	INSERT INTO SURGEON (surgeonid,fname,lname,description)
	VALUES  ('butLR','Linda','Ramos','Orthopedic');
	INSERT INTO SURGEON (surgeonid,fname,lname,description)
	VALUES  ('GIDon','Donald','Yang','Gastroenterologist');
	
	--inserting values into Patient table
	INSERT INTO PATIENT(PatientID,fname,lname,dateofbirth,insurance)
	VALUES('GJ280905','Garret','Johnson','28-SEP-2005','Pacific Source');
	INSERT INTO PATIENT(PatientID,fname,lname,dateofbirth,insurance)
	VALUES('VC280805','Castaldo','Vincenzo','28-AUG-2005','Pacific Source');
	INSERT INTO PATIENT(PatientID,fname,lname,dateofbirth,insurance)
	VALUES('LY271105','Lianna','Yang','27-NOV-05','Pacific Source');
	INSERT INTO PATIENT(PatientID,fname,lname,dateofbirth,insurance)
	VALUES('RN052705','Ryan','Nag','27-MAY-05','Pacific Source');
	INSERT INTO PATIENT(PatientID,fname,lname,dateofbirth,insurance)
	VALUES('MR011675','Marcela','Rosenberger','16-JAN-75','Pacific Source');
	COMMIT;	
		
	--inserting values into PROVIDER table
CREATE TABLE ANESTHESIA_PROVIDER
(ProviderID  VARCHAR(8)NOT NULL PRIMARY KEY,
fname VARCHAR(20) NOT NULL,
lname VARCHAR (20)NOT NULL,
title VARCHAR (6) NOT NULL
 );
 

INSERT INTO ANESTHESIA_PROVIDER (ProviderID,fname,lname,Title)
VALUES ('GasSA','Steve','Aufd','MD');	
INSERT INTO ANESTHESIA_PROVIDER (ProviderID,fname,lname,Title)
VALUES ('GasHS','Hannan','Salhi','MD');	
INSERT INTO ANESTHESIA_PROVIDER (ProviderID,fname,lname,Title)
VALUES ('GasSS','Samaneh','Saa','CRNA');	
COMMIT;

INSERT INTO FACILITY(FacilityID,Name)ses
	VALUES (22222,'Eye Clinic');
	
	INSERT INTO Facility(FacilityID,Name)
	VALUES(333333,'Hip & Knee Clinic');
	
	INSERT INTO FACILITY(FacilityID,Name)
	VALUES(444444,'GI Eugene');		


INSERT INTO ANESTHESIA_LOG (CaseID, StartTime,ProviderID,EndTime)
	VALUES(1058,TO_DATE('2012-JUN-06 8:50','YYYY-MON-DD HH24:MI','NLS_DATE_LANGUAGE=AMERICAN'),'GasNC',
	       TO_DATE('2012-JUN-04 9:30','YYYY-MON-DD HH24:MI','NLS_DATE_LANGUAGE=AMERICAN'));


INSERT INTO Surg_procedure(ProcedureID,Name)
	VALUES('Hip1111','Ilizarov fixator and monolateral fixators');

DROP TABLE SURG_PROCEDURE;

CREATE TABLE SURG_PROCEDURE
(ProcedureID VARCHAR(8)NOT NULL PRIMARY KEY,
Name VARCHAR(50) NOT NULL 
);

INSERT INTO Surg_procedure(ProcedureID,Name)
	VALUES('Hip1111','Ilizarov fixator and monolateral fixators');
INSERT INTO Surg_procedure(ProcedureID,Name)
	VALUES('Nerv1111','Nerve repair');
INSERT INTO Surg_procedure(ProcedureID,Name)
	VALUES('Micro1','Microvascular free flaps');
INSERT INTO Surg_procedure(ProcedureID,Name)	
	VALUES('Lap002','laparoscopic appendectomy');

	
UPDATE  CASE
	SET ProcedureID='Hip1111'
	WHERE CaseID=1005;
	

UPDATE  CASE
	SET ProcedureID='Hip1111'
	WHERE CaseID=1020;
	

UPDATE  CASE
	SET ProcedureID='Nerv1111'
	WHERE CaseID=1025;
	
	

UPDATE  CASE
	SET ProcedureID='Micro1'
	WHERE CaseID=1024;
	
	
	

UPDATE  CASE
	SET ProcedureID='Micro1'
	WHERE CaseID=1024;
	
UPDATE CASE
		SET ProcedureID= 'Lap002'
		WHERE CaseID=1041;
	

UPDATE  CASE
	SET ProcedureID='Nerv1111'
	WHERE CaseID=1023;
---
DROP TABLE VITAL_SIGNS;

 


 
 --INSERT INTO MEDICATION THERAPY
	INSERT INTO MEDICATION_THERAPY(medtherapyid,medtime,caseid,dosage,units, medicationid,route)
	VALUES(0001,TO_DATE('2012-JUN-12 8:50','YYYY-MON-DD HH24:MI','NLS_DATE_LANGUAGE=AMERICAN'),'1059','100','mg','roc','IV');
	
 
 
 --INSERT INTO MEDICATION THERAPY
	INSERT INTO MEDICATION_THERAPY(medtherapyid,medtime,caseid,dosage,units, medicationid,route)
	VALUES(0002,TO_DATE('2012-JUN-06 7:50','YYYY-MON-DD HH24:MI','NLS_DATE_LANGUAGE=AMERICAN'),'1059','100','mg','roc','IV');
	
	
	('01-APR-2010','1058','100mg','roc','IV');
 --Decided to change MedicationID to VARCHAR
 
 




 

		
	--inserting values into Facility table
	
	
INSERT INTO VITAL_SIGNS (vitalstime,CaseID,heart_rate,spo2,bp_systolic,bp_diastolic)
VALUES(TO_DATE('2012-JUN-04 7:30','YYYY-MON-DD HH24:MI','NLS_DATE_LANGUAGE=AMERICAN'),1005,62,99,160,75);

INSERT INTO VITAL_SIGNS (vitalstime,CaseID,heart_rate,spo2,bp_systolic,bp_diastolic)
VALUES(TO_DATE('2012-JUN-04 7:45','YYYY-MON-DD HH24:MI','NLS_DATE_LANGUAGE=AMERICAN'),1005,62,99,160,75);

INSERT INTO VITAL_SIGNS (vitalstime,CaseID,heart_rate,spo2,bp_systolic,bp_diastolic)
VALUES(TO_DATE('2012-JUN-04 8:45','YYYY-MON-DD HH24:MI'),1005,75,99,118,62);


INSERT INTO VITAL_SIGNS (vitalstime,CaseID,heart_rate,spo2,bp_systolic,bp_diastolic)
VALUES(TO_DATE('2011-JUL-2011 8:00','YYYY-MON-DD HH24:MI'),1005,75,99,118,62);


INSERT INTO VITAL_SIGNS (vitalstime,CaseID,heart_rate,spo2,bp_systolic,bp_diastolic)
VALUES(TO_DATE('2011-JUL-03 7:45','YYYY-MON-DD HH24:MI','NLS_DATE_LANGUAGE=AMERICAN'),1041,62,99,160,75);

INSERT INTO VITAL_SIGNS (vitalstime,CaseID,heart_rate,spo2,bp_systolic,bp_diastolic)
VALUES(TO_DATE('2011-JUL-03 8:00','YYYY-MON-DD HH24:MI','NLS_DATE_LANGUAGE=AMERICAN'),1041,65,100,160,78);


INSERT INTO VITAL_SIGNS (vitalstime,CaseID,heart_rate,spo2,bp_systolic,bp_diastolic)
VALUES(TO_DATE('2011-JUL-03 8:30','YYYY-MON-DD HH24:MI','NLS_DATE_LANGUAGE=AMERICAN'),1041,68,100,170,78);

INSERT INTO ANESTHESIA_LOG(CaseID,StartTime,ProviderID,EndTime)
VALUES(1059,TO_DATE('2012-JUN-12 7:45','YYYY-MON-DD HH24:MI','NLS_DATE_LANGUAGE=AMERICAN'),
'GasNC',TO_DATE('2012-JUN-12 8:30','YYYY-MON-DD HH24:MI','NLS_DATE_LANGUAGE=AMERICAN'));


--***************************************************************END OF TABLES CREATION************************************


--------------------------------------------------BEGINNING  OF STORED PROCEDURES AND FUNCTIONS*******************************************
CREATE SEQUENCE proc_seq start with 20;

--This procedures inserts data in the case table, the caseID is generated by the CaseID_seq.
CREATE SEQUENCE CaseID_seq start with 1057;

CREATE OR REPLACE PROCEDURE prod_addcase_sp--declaring procedure and in parameters
	(patient IN PATIENT.PatientID%TYPE,
	 surgeon IN SURGEON.SurgeonID%TYPE,
	 casedate IN DATE,
	 facility IN FACILITY.FacilityID%TYPE,
	 oroom IN  NUMBER,
	 proced IN VARCHAR
	  )
	IS
BEGIN
	INSERT INTO CASE (CaseID,PatientID,SurgeonID,casedate,FacilityID,ORnumber,procedureid)--,ProviderID
		VALUES(CaseID_seq.NEXTVAL,patient,surgeon,casedate,facility,oroom,proced);--idproduct is autogenerated by sequence--,provider
COMMIT;
END;
/

--testing procedure 
 INSERT INTO CASE(CaseID,PatientID,SurgeonID,casedate,FacilityID,ORnumber,procedureid)
	        VALUES(1057,'VC280805','butLR','12-JUN-2012',333333,00001,'Micro1')
	--adding records into case table by using the procedure prod_addcase_sp	
	EXECUTE prod_addcase_sp('GJ280905','butLR',SYSDATE,333333,00001,'Micro1');
	EXECUTE prod_addcase_sp('GJ280905','butLR','06-JUN-2012',333333,00001,'Micro1');
	EXECUTE prod_addcase_sp('VC280805','butLR',SYSDATE,333333,00001,'Nerv1111');
	EXECUTE prod_addcase_sp('RN052705','butLR','03-jun-2011',333333,00001, 'Micro1');
	EXECUTE prod_addcase_sp('LY271105','butLR','03-JAN-2011',333333,00001,'Hip1111');
	EXECUTE prod_addcase_sp('LY271105','butLR','03-JAN-2011',333333,00001,'Lap002');
	
	INSERT INTO CASE
	
--the following procedure inserts data into the vital signs table 

CREATE OR REPLACE PROCEDURE vitalsigns_add_sp--declaring procedure and in parameters
	(inputtime IN VARCHAR2,--using varchar but will change into DATE when inserting
	 caseID IN VITAL_SIGNS.CaseID%TYPE,
	 heart IN VITAL_SIGNS.Heart_rate%TYPE,
	 spo2 IN VITAL_SIGNS.SPO2%TYPE,
	 bpsys IN VITAL_SIGNS.bp_systolic%TYPE,
	bpdias IN VITAL_SIGNS.bp_diastolic%TYPE
	 )
	IS
BEGIN
	INSERT INTO VITAL_SIGNS (vitalstime,CaseID,heart_rate,spo2,bp_systolic,bp_diastolic)
	VALUES(TO_DATE(inputtime,'YYYY-MON-DD HH24:MI','NLS_DATE_LANGUAGE=AMERICAN'),caseID,heart,spo2,bpsys,bpdias);
COMMIT;
END;
/


--testing procedure
EXECUTE vitalsigns_add_sp('2001-JUN-12 7:30',1057,65,99,110,68);
EXECUTE vitalsigns_add_sp('2011-JUN-06 8:56',1058,78,99,110,68);

EXECUTE vitalsigns_add_sp('2012-JUN-12 7:30',1059,65,98,100,68);

EXECUTE vitalsigns_add_sp('2012-JUN-03 8:00',1060,70,99,105,69);
EXECUTE vitalsigns_add_sp('2012-JUN-12 8:45',1059,70,99,105,69);




--show errors PROCEDURE vitalsigns_add_sp;

--EXECUTE vitalsigns_add_sp('2012-JUN-06 12:00:00',1046,70,99,190,100);
------


--this function will find the date or dates  that person had surgical procedures , the function will find the patient id first and then will return the value as a 
---cursor with all the procedure dates the patient has had

 CREATE OR REPLACE FUNCTION findCaseDate_sf
    (last IN patient.lname%TYPE,
     first IN patient.fname%TYPE,
     dob IN patient.dateofbirth%TYPE
     )
      RETURN SYS_refcursor --returning a record because in some cases there will be more thatn one record for a patient
 IS
    date_cursor SYS_REFCURSOR;
	lv_id patient.patientid%TYPE;
 BEGIN
     SELECT patientid
      INTO lv_id
       FROM patient
        WHERE fname=first 
         AND lname=last
         AND dateofbirth=dob;
    OPEN date_cursor FOR 
     SELECT casedate
      FROM case
       WHERE patientid=lv_id;
    RETURN date_cursor;
	EXCEPTION
	  WHEN NO_DATA_FOUND THEN
	  DBMS_OUTPUT.PUT_LINE('No Data found for that patient');
	  WHEN INVALID_CURSOR THEN
	  DBMS_OUTPUT.PUT_LINE('No Data found for that patient');
	  RETURN date_cursor;
END;
 /
 
 

--show erros FUNCTION findCaseDate_sf;

  

 
 --testing function
--Declaring  the cursor that will hold returned value

--verifying that exception works 
DECLARE
lv_apellido VARCHAR(20):='Johnson';
lv_nombre  VARCHAR(20):='Garret';
lv_fechanac DATE :='28-SEP-05';
lv_date DATE;
lv_returned SYS_REFCURSOR;
BEGIN
     lv_returned :=findCaseDate_sf(lv_apellido,lv_nombre,lv_fechanac);
    LOOP
        FETCH lv_returned INTO lv_date;
        EXIT WHEN lv_returned %NOTFOUND;
        DBMS_OUTPUT.PUT_LINE(lv_date || '     ' ||lv_nombre);
    END LOOP;
	CLOSE lv_returned;
END;
/




-------------------

	
--cascade delete		
--This procedure deletes a case record from the CASE table and its related tables medication_therapy,vital_signs 
CREATE OR REPLACE PROCEDURE prod_deletecase_sp
    (idcase IN NUMBER)
    IS
	lv_id NUMBER;
BEGIN
	SELECT caseid
	INTO lv_id
	from case
	where caseid=idcase;
    DELETE FROM MEDICATION_THERAPY
	WHERE CaseID=lv_id;
	DELETE FROM VITAL_SIGNS
	WHERE CaseID=lv_id;
	DELETE FROM CASE
	WHERE CaseID=lv_id;
COMMIT;
END;
/
	
	
EXECUTE prod_deletecase_sp(1057);


--show errors PROCEDURE prod_addcase_sp;

--UPDATE procedure


--This procedure UPDATE patient information , it changes or adds insurance , first we get patient id, 
CREATE OR REPLACE PROCEDURE updateInsurance_sp
    (last IN patient.lname%TYPE,
     first IN patient.fname%TYPE,
     dob IN patient.dateofbirth%TYPE,
	 insinfo IN patient.insurance%TYPE
	 )
     
 IS
   	lv_id patient.patientid%TYPE;
 BEGIN
     SELECT patientid
    INTO lv_id
    FROM patient
    WHERE fname=first 
    AND lname=last
    AND dateofbirth=dob;
		UPDATE PATIENT
		SET insurance=insinfo
		WHERE patientid=lv_id;
	EXCEPTION
	   WHEN NO_DATA_FOUND THEN
	      DBMS_OUTPUT.PUT_LINE('There is no data for that patient, You might have a spelling error');
    COMMIT;
END;
 /


 --testing
 EXECUTE updateInsurance_sp('Johnson','Garret','28-SEP-05','Blue Cross');
 
 --verifying that data was updated 
select * from patient
where lname='Johnson' 
AND fname='Garret' 
AND dateofbirth='28-SEP-05';

------------end of testing 

	
	

--show errors PROCEDURE prod_addcase_sp

	
	
	
	
--This procedure deletes a case record from the CASE table and its related tables medication_therapy,vital_signs	
CREATE OR REPLACE PROCEDURE prod_deletecase_sp
    (caseid IN NUMBER)
    IS
BEGIN
	DELETE FROM MEDICATION_THERAPY
	WHERE CaseID=caseid;
	DELETE FROM VITAL_SIGNS
	WHERE CaseID=caseid;
	DELETE FROM CASE
	WHERE CaseID=caseid;
--COMMIT;
END;
/
	
	
EXECUTE prod_deletecase_sp(1053);

--this procedure deletes a case record from the CASE table
show errors PROCEDURE prod_addcase_sp;


	/* this are my constraints  caseID children

	 CONSTRAINT ther_caseid_FK FOREIGN KEY(CaseID)
   REFERENCES CASE(CaseID),
  
   CONSTRAINT vs_caseid_FK FOREIGN KEY(CaseID)
   REFERENCES CASE(CaseID)
 */
 
  



--TO_DATE('2011-JUL-03 8:00','YYYY-MON-DD HH24:MI','NLS_DATE_LANGUAGE=AMERICAN')
------


--The following function will return the cases and dates that  a provider has worked at in a given period of time

 CREATE OR REPLACE FUNCTION providerCases_sf
    (AnesID IN PROVIDER.ProviderID%TYPE,
	 StDate IN DATE ,
	 EndDate IN DATE
    )
      RETURN SYS_refcursor 
 IS
    date_cursor SYS_REFCURSOR;
 BEGIN
    OPEN date_cursor FOR 
    SELECT c.caseID,PatientID,SurgeonID,casedate,FacilityID,ornumber,ProcedureID,providerid
    FROM case c, anesthesia_log a
    WHERE ProviderID=AnesID
	AND c.caseID=a.CaseID
	AND casedate between stDate and endDate;
    RETURN date_cursor;
 END;
 /
 

----

--TESTING FUNCTION 


VARIABLE  c refcursor 
--this cursor will hold the value returned by the function 

DECLARE 
lv_anesthesia PROVIDER.ProviderID%TYPE:='GasNC'; 
lv_start DATE:='01-JAN-00';
lv_end DATE:='15-JUN-12';
begin
  select providerCases_sf(lv_anesthesia,lv_start,lv_end)  --select statement calls the function to get the cursor 
  INTO :c 
  from dual;
end;
/

---
PRINT :C;

select 

-----------------checking that we got the right values 

SELECT c.caseID,PatientID,SurgeonID,casedate,FacilityID,ornumber,ProcedureID,providerid
    FROM case c, anesthesia_log a
    WHERE c.caseID=a.CaseID
	AND ProviderId='GasNC'
	AND casedate between '01-JAN-10'  and '05-JUN-12';
	
	
	
SELECT * FROM CASE 
WHERE  casedate BETWEEN  '01-JAN-10' AND '05-JUN-12'
AND  ProviderID='GasSA';
------
 COLUMN vitalstime  FORMAT A20;

 
 
 
 --------------
 

---the following  PROCEDURE  will return a set of records for a caseid,  using caseId as parameter
 
  --
 
 CREATE OR REPLACE PROCEDURE caseReport_sp
    (casenumber IN NUMBER )
  IS
    CURSOR cur_report IS
		 select  c.caseid,c.patientid , surgeonid,  facilityid,ornumber, procedureid,
		          vitalstime, heart_rate, spo2, bp_systolic, bp_diastolic,a.ProviderID,a.StartTime
		FROM case c, vital_signs v, anesthesia_log a
	    WHERE c.caseid=v.caseid
	    AND c.caseid=casenumber
	    AND c.caseid=a.caseid
	    ORDER by vitalstime ;
	 BEGIN
	DBMS_OUTPUT.PUT_LINE('patientid     facilityid     or      procedureid    surgeonid    providerid           vitalstime           heart_rate     spo2    bp_systolic    bp_dyastolic');
	DBMS_OUTPUT.NEW_LINE;
	DBMS_OUTPUT.PUT_LINE('--------------------------------------------------------------------------------------------------------------------------------------------------------------');
    FOR rec_report IN  cur_report  
	LOOP
		DBMS_OUTPUT.PUT_LINE(rec_report.patientid||'     '||rec_report.facilityid||'        '|| rec_report.ornumber  || '          '|| rec_report.Procedureid   || '        '|| rec_report.surgeonid  ||  '       '||
		rec_report.ProviderID|| '       '|| rec_report.vitalstime    || '     '|| rec_report.heart_rate  ||'     '||rec_report.spo2    ||'     '||
		rec_report.bp_systolic  ||'     '||rec_report.bp_diastolic);
		
		END LOOP;
	COMMIT;
END;
/
		
   COLUMN vitalstime  FORMAT A20;
 EXECUTE caseReport_sp(1059);
/* **************************************************************************************************************************************************************

																   PACKAGES
************************************************************************************************************************************************************/
--The Following package  gets all records to a caseid,  we get the case ID(s) through a function that uses first name, last name and dob as parameters,   then we can use caseid
--to retrieve information 

CREATE OR REPLACE PACKAGE caseinfo_pkg
IS
FUNCTION findCaseDate_sf
    (last IN patient.lname%TYPE,
     first IN patient.fname%TYPE,
     dob IN patient.dateofbirth%TYPE
     )
      RETURN SYS_refcursor ;
PROCEDURE caseReport_sp
    (casenumber IN NUMBER );
END;
/

CREATE OR REPLACE PACKAGE BODY caseinfo_pkg IS
 FUNCTION findCaseDate_sf
    (last IN patient.lname%TYPE,
     first IN patient.fname%TYPE,
     dob IN patient.dateofbirth%TYPE
     )
      RETURN SYS_refcursor 
 IS
    date_cursor SYS_REFCURSOR;
	lv_id patient.patientid%TYPE;
 BEGIN
     SELECT patientid
      INTO lv_id
       FROM patient
        WHERE fname=first 
         AND lname=last
         AND dateofbirth=dob;
    OPEN date_cursor FOR 
     SELECT casedate
      FROM case
       WHERE patientid=lv_id;
    RETURN date_cursor;
	EXCEPTION
	  WHEN NO_DATA_FOUND THEN
	  DBMS_OUTPUT.PUT_LINE('No Data found');
END findCaseDate_sf;
PROCEDURE caseReport_sp
    (casenumber IN NUMBER )
  IS
    CURSOR cur_report IS
		 select  c.caseid,c.patientid , surgeonid,  facilityid,ornumber, procedureid,
		          vitalstime, heart_rate, spo2, bp_systolic, bp_diastolic,a.ProviderID,a.StartTime
		FROM case c, vital_signs v, anesthesia_log a
	    WHERE c.caseid=v.caseid
	    AND c.caseid=casenumber
	    AND c.caseid=a.caseid
	    ORDER by vitalstime ;
	 BEGIN
	DBMS_OUTPUT.PUT_LINE('patientid     facilityid     or      procedureid    surgeonid    providerid           vitalstime           heart_rate     spo2    bp_systolic    bp_dyastolic');
	DBMS_OUTPUT.NEW_LINE;
	DBMS_OUTPUT.PUT_LINE('--------------------------------------------------------------------------------------------------------------------------------------------------------------');
    FOR rec_report IN  cur_report  
	LOOP
		DBMS_OUTPUT.PUT_LINE(rec_report.patientid||'     '||rec_report.facilityid||'        '|| rec_report.ornumber  || '          '|| rec_report.Procedureid   || '        '|| rec_report.surgeonid  ||  '       '||
		rec_report.ProviderID|| '       '|| rec_report.vitalstime    || '     '|| rec_report.heart_rate  ||'     '||rec_report.spo2    ||'     '||
		rec_report.bp_systolic  ||'     '||rec_report.bp_diastolic);
		
		END LOOP;
	COMMIT;
	END caseReport_sp;
END;
/


--Invoking packaged procedure
EXECUTE caseinfo_pkg.caseReport_sp(1059);

--verifying that packaged function works 

DECLARE
lv_apellido VARCHAR(20):='Johnson';
lv_nombre  VARCHAR(20):='Garret';
lv_fechanac DATE :='28-SEP-05';
lv_date DATE;
lv_returned SYS_REFCURSOR;
BEGIN
     lv_returned :=caseinfo_pkg.findCaseDate_sf(lv_apellido,lv_nombre,lv_fechanac);
    LOOP
        FETCH lv_returned INTO lv_date;
        EXIT WHEN lv_returned %NOTFOUND;
        DBMS_OUTPUT.PUT_LINE(lv_date || '     ' ||lv_nombre);
    END LOOP;
	CLOSE lv_returned;
END;
/



------------SECOND PACKAGE

--package specification
CREATE OR REPLACE PACKAGE vitals_pkg
IS 
  FUNCTION earliestvs_sf
  (idcase IN VITAL_SIGNS.CaseID%TYPE)
  RETURN VARCHAR2;
  PROCEDURE vitalsigns_add_sp--declaring procedure and in parameters
  (inputtime IN VARCHAR2,--using varchar but will change into DATE when inserting
	 caseID IN VITAL_SIGNS.CaseID%TYPE,
	 heart IN VITAL_SIGNS.Heart_rate%TYPE,
	 spo2 IN VITAL_SIGNS.SPO2%TYPE,
	 bpsys IN VITAL_SIGNS.bp_systolic%TYPE,
	bpdias IN VITAL_SIGNS.bp_diastolic%TYPE
   );
 END;
 /

--Package body

CREATE OR REPLACE PACKAGE BODY vitals_pkg IS
------------This function returns the earliest time when vitals signs were entered in the system
 FUNCTION earliestvs_sf
(idcase IN VITAL_SIGNS.CaseID%TYPE)
RETURN VARCHAR2
IS
lv_earliesttime DATE;
BEGIN
	SELECT MIN(vitalstime) --TO_DATE(vitalstime,'YYYY-MON-DD HH24:MI')
	INTO lv_earliesttime
	FROM vital_signs
	WHERE CaseID=idcase;
	RETURN TO_CHAR(lv_earliesttime,'YYYY-MON-DD HH24:MI');
   END earliestvs_sf ;
PROCEDURE vitalsigns_add_sp--declaring procedure and in parameters
	(inputtime IN VARCHAR2,--using varchar but will change into DATE when inserting
	 caseID IN VITAL_SIGNS.CaseID%TYPE,
	 heart IN VITAL_SIGNS.Heart_rate%TYPE,
	 spo2 IN VITAL_SIGNS.SPO2%TYPE,
	 bpsys IN VITAL_SIGNS.bp_systolic%TYPE,
	bpdias IN VITAL_SIGNS.bp_diastolic%TYPE
	 )
	IS
BEGIN
	INSERT INTO VITAL_SIGNS (vitalstime,CaseID,heart_rate,spo2,bp_systolic,bp_diastolic)
	VALUES(TO_DATE(inputtime,'YYYY-MON-DD HH24:MI','NLS_DATE_LANGUAGE=AMERICAN'),caseID,heart,spo2,bpsys,bpdias);
COMMIT;
END vitalsigns_add_sp;
END;
/


------testing function 
--declaring anonymous block
DECLARE 
lv_timeresult VARCHAR2(30);
BEGIN
lv_timeresult:=vitals_pkg.earliestvs_sf(1059);--calling packaged function
DBMS_OUTPUT.PUT_LINE(lv_timeresult);
END;
/
DE
--calling packaged procedure
EXECUTE vitals_pkg.vitalsigns_add_sp('2012-JUN-04 9:00',1059,70,99,190,100);

EXECUTE vitals_pkg.vitalsigns_add_sp('03-JUL-11 9:00',1041,71,100,105,69);

SELECT * FROM vital_signs;

/* ***********************************************************************

						BEGINNING OF TRIGGERS
						
 /*************************************************************************

	 
--The Following trigger fires when  an user to change the first name of a patient in a log table , the trigger updates the name 
in the Patient table, the purpose of this table is to keep track 
of the changes if there was a mistake when the data was entered in the patient table */

CREATE OR REPLACE TRIGGER name_change_trg
AFTER INSERT   ON NAMECHANGE_LOG--trigger will fire after insert
FOR EACH ROW
BEGIN
UPDATE PATIENT
SET FName=:NEW.FName,
	LName=:NEW.LName
WHERE PatientID= :NEW.PatientID;
END;
/
	

	
-------Testing trigger by inserting values into the namechange_log table	
  select * 
  from patient
  where patientid='GJ280905';
  
INSERT INTO NAMECHANGE_LOG(LogID,PatientID,FName,LName,ChangeDate)
	VALUES(LogID_seq.NEXTVAL,'GJ280905','Garret','Johnson','12-JUN-12');
	
select * 
  from patient
  where patientid='GJ280905';
  
  ---end of testing

 INSERT INTO MEDICATION_THERAPY(medtherapyid,medtime,caseid,dosage,units, medicationid,route)
   VALUES(0001,TO_DATE('2012-JUN-12 8:50','YYYY-MON-DD HH24:MI','NLS_DATE_LANGUAGE=AMERICAN'),'1059','100','mg','roc','IV');

 
--------------The following trigger fires when we delete a record from medication_therapy table and adds a record into the med_log table
CREATE OR REPLACE TRIGGER medlog_trg
BEFORE DELETE ON MEDICATION_THERAPY
FOR EACH ROW
 DECLARE
  mutating_table EXCEPTION;--I create an exception to prevent the mutating table error 
  PRAGMA EXCEPTION_INIT(mutating_table,-4091);
BEGIN
 INSERT INTO MEDICATION_LOG VALUES(:OLD.MedtherapyID,SYSDATE,:OLD.MedtherapyID);;
EXCEPTION
WHEN mutating_table THEN
null;
END;
/


--testing  
select * from medication_log;

DELETE  FROM MEDICATION_THERAPY WHERE medtherapyid=0001;

select * from medication_log;


------UPDATE TRIGGER,   This trigger will insert values into the hospital employee table ,  the trigger fires when the lname column
--of the surgeon table gets updated, for example when someone gets married and changes their last name.

CREATE OR REPLACE TRIGGER employee_trg
BEFORE UPDATE OF Lname ON SURGEON
FOR EACH ROW
DECLARE
	inputtime DATE;
BEGIN
 INSERT INTO  Hospital_Emp 
 VALUES (Emp_sequ.NEXTVAL,:OLD.fname,:NEW.lname,:OLD.SurgeonID);
END;
/


--testing   
select * from surgeon;
select * from hospital_emp;


update SURGEON
SET lname='Ramos'
WHERE surgeonid='butLR';

select * from hospital_emp;